package devassestment.mauriciobatlle.halifax;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import devassestment.mauriciobatlle.halifax.beans.Brand;
import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;
import devassestment.mauriciobatlle.halifax.datasource.Branches;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ObjectIntegrityTests {
	OriginalResponse testObject;
	Brand halifax;
	Long realcount;
	@Before
	public void setUp() throws Exception {
		testObject = Branches.getObjectFromWS();
		halifax= testObject.getData().get(0).getBrand().get(0);
		realcount=(long) testObject.getData().get(0).getBrand().get(0).getBranch().size();
	}
	

	@Test
	public void checkMetaValuesTest() {
		assertThat(testObject.getMeta().getLicense().equals("https://www.openbanking.org.uk/open-licence"));
		assertThat(testObject.getMeta().getAgreement().equals("Use of the APIs and any related data will be subject to the terms of the Open Licence and subject to terms and conditions"));		
		assertThat(testObject.getMeta().getTermsOfUse().equals("https://www.openbanking.org.uk/terms"));		
		assertNotNull(testObject.getMeta().getTotalResults());
		assertNotNull(testObject.getMeta().getLastUpdated());	
		
	}
	
	@Test
	public void checkDataStructure() {
		assertThat(testObject.getData().get(0).getBrand().get(0).getBrandName().equals("Halifax"));
		assertThat(realcount.equals(testObject.getMeta().getTotalResults()));		
	}
	
	@Test
	public void checkBranchData() {
		assertNotNull(halifax.getBranch().get(0).getName());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getGeoLocation().getGeographicCoordinates().getLatitude());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getGeoLocation().getGeographicCoordinates().getLongitude());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getAddressLine());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getTownName());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getCountrySubDivision().get(0));
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getCountry());
		assertNotNull(halifax.getBranch().get(0).getPostalAddress().getPostCode());
	}
	
	
	

}
