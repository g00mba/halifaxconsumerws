package devassestment.mauriciobatlle.halifax;

import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static devassestment.mauriciobatlle.halifax.utils.CustomObjectHelper.createCustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.CustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.Branch;
import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;
import devassestment.mauriciobatlle.halifax.datasource.Branches;

public class CheckAavriObject {
	OriginalResponse testObject;
	Branch sampleBranch;
	private static final  Logger aavriTestObjectLogger=LogManager.getLogger(CheckAavriObject.class);
	@Before
	public void setUp() throws Exception {
		testObject = Branches.getObjectFromWS();
		sampleBranch= testObject.getData().get(0).getBrand().get(0).getBranch().get(1);
	}



	@Test
	public void ObjectCreationTest() {
		CustomBranchLocation testLocation = createCustomBranchLocation(sampleBranch);
		assertNotNull(testLocation);
		aavriTestObjectLogger.info("branch name:"+testLocation.getBranchName());
		aavriTestObjectLogger.info("latitude: "+testLocation.getLatitude());
		aavriTestObjectLogger.info("longitude: "+testLocation.getLongitude());
		aavriTestObjectLogger.info("city: "+testLocation.getCity());
		aavriTestObjectLogger.info("country: "+testLocation.getCountry());
		aavriTestObjectLogger.info("postal code: "+testLocation.getPostCode());
		
	}
	


}
