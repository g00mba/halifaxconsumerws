/**
 * 
 */
package devassestment.mauriciobatlle.halifax;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import java.util.List;
import java.util.ListIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static devassestment.mauriciobatlle.halifax.utils.CustomObjectHelper.*;

import devassestment.mauriciobatlle.halifax.beans.CustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.CustomWSResponseEntity;
import devassestment.mauriciobatlle.halifax.beans.Branch;
import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;
import devassestment.mauriciobatlle.halifax.datasource.Branches;

public class CheckAavriWsResponse {

	OriginalResponse testObject;
	List<Branch> sampleBranches;
	private static final Logger aavriTestObjectLogger = LogManager.getLogger(CheckAavriObject.class);

	@Before
	public void setUp() throws Exception {
		testObject = Branches.getObjectFromWS();
		sampleBranches = testObject.getData().get(0).getBrand().get(0).getBranch();
	}

	@Test
	public void checkStandardAavriResponse() {
		aavriTestObjectLogger.info("Checking Standard response...");
		CustomWSResponseEntity payload = createCustomWsResponse(testObject);
		assertNotNull(payload.getBranchLocations());
		Long payloadCount = Long.valueOf(payload.getBranchLocations().size());
		aavriTestObjectLogger.info("payload count to be delivered: " + payloadCount);
		aavriTestObjectLogger.info("original payload count: " + testObject.getMeta().getTotalResults());
		assertThat(payloadCount.equals(testObject.getMeta().getTotalResults()));

	}

	@Test
	public void checkFilteredAavriResponse() {
		aavriTestObjectLogger.info("checking filtered response...");
		applyCityFilter("Liverpool", testObject);
		CustomWSResponseEntity payload = createCustomWsResponse(testObject);
		assertNotNull(payload.getBranchLocations());
		Long payloadCount = Long.valueOf(payload.getBranchLocations().size());
		aavriTestObjectLogger.info("payload count to be delivered: " + payloadCount);
		aavriTestObjectLogger.info("original payload count: " + testObject.getMeta().getTotalResults());
		ListIterator<CustomBranchLocation> iterator = payload.getBranchLocations().listIterator();
		while (iterator.hasNext()) {
			assertThat(iterator.next().getBranchName().equalsIgnoreCase("LIVERPOOL"));
		}

	}

}
