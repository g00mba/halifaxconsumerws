package devassestment.mauriciobatlle.halifax;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;


import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;
import devassestment.mauriciobatlle.halifax.datasource.Branches;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataFetchTest {
	@Test
	public void endpointConnectionTest() {
		ResponseEntity <String> response = Branches.endpointFetch();
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		
	}
	
	@Test
	public void ObjectCreationTest() {
		OriginalResponse testObject = Branches.getObjectFromWS();
		assertNotNull(testObject);		
	}
	

}


