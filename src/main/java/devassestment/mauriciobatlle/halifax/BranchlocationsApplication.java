package devassestment.mauriciobatlle.halifax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BranchlocationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BranchlocationsApplication.class, args);
	}
}
