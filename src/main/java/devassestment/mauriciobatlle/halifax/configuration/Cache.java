package devassestment.mauriciobatlle.halifax.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import static devassestment.mauriciobatlle.halifax.datasource.Branches.getObjectFromWS;

import static devassestment.mauriciobatlle.halifax.utils.CustomObjectHelper.createCustomWsResponse;

@Configuration
@EnableCaching
@EnableScheduling
public class Cache {

	private static final Logger cacheLogger = LogManager.getLogger(Cache.class);

	@Bean
	public CacheManager branchesCacheManager() {
		return new ConcurrentMapCacheManager("originalSource", "processedResponse", "filteredResponse");
	}

//each cache can be set to different refresh periods to cater for different usage loads
//the short refresh periods are just to show the functionality, since this is a location ws, chances are we could have refresh periods of hours
	@Scheduled(fixedDelay = 240000)
	@CacheEvict(cacheNames = "originalSource", allEntries = true)
	public void clearMainCache() {
		cacheLogger.info("refreshing Halifax Data in cache...");
		createCustomWsResponse(getObjectFromWS());
	}

	@Scheduled(fixedDelay = 120000)
	@CacheEvict(cacheNames = ("processedResponse"), allEntries = true)
	public void clearResponseCache() {
		cacheLogger.info("purging cache data from standard endpoint...");
	}

	@Scheduled(fixedDelay = 180000)
	@CacheEvict(cacheNames = ("filteredResponse"), allEntries = true)
	public void clearFilteredResponseCache() {
		cacheLogger.info("purging cache data from filtered endpoint...");

	}

}
