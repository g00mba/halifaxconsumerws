package devassestment.mauriciobatlle.halifax.datasource;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;

@Service
public class Branches {

	private static final Logger endpointLogger = LogManager.getLogger(Branches.class);

	public static final ResponseEntity<String> endpointFetch() {
		endpointLogger.info("attempting to fecth data from halifax...");
		RestTemplate restDoc = new RestTemplate();
		ResponseEntity<String> response = null;
		try {
			String endpoint = "https://api.halifax.co.uk/open-banking/v2.2/branches";
			response = restDoc.getForEntity(endpoint, String.class);
		} catch (RestClientException e) {
			endpointLogger.fatal("failed to fetch data from halifax endpoint: %s" , e.getMessage());
		}

		return response;

	}

	@Cacheable(value = "originalSource", sync = true)
	public static final OriginalResponse getObjectFromWS() {
		endpointLogger.info("attempting to parse data obtained from endpoint...");
		ObjectMapper mapper = new ObjectMapper();
		OriginalResponse fetchObject = null;
		try {
			fetchObject = mapper.readValue(endpointFetch().getBody(), OriginalResponse.class);
		} 
		
		catch (JsonParseException e) {
			endpointLogger.error("failed to parse data from JSON response: %s", e.getMessage());
		}
		
		catch (JsonMappingException e) {
			endpointLogger.error("failed to map data from JSON response: %s", e.getMessage());
		}
		
		catch (IOException e) {
			endpointLogger.error("failed to process data, IO Exception: %s", e.getMessage());
		}
		
		catch (NullPointerException e) {
			endpointLogger.error("Unknown error detected, a null object was returned by the original source parser");
		}
		

		return fetchObject;
	}

}
