package devassestment.mauriciobatlle.halifax.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ 
	"locations" 
	})
public class CustomWSResponseEntity {
	
	@JsonProperty("locations")
	private List <CustomBranchLocation> branchLocations;
	
	@JsonProperty("locations")
	public List<CustomBranchLocation> getBranchLocations() {
		return branchLocations;
	}

	@JsonProperty("locations")
	public void setBranchLocations(List<CustomBranchLocation> branchLocations) {
		this.branchLocations = branchLocations;
	}

}
