package devassestment.mauriciobatlle.halifax.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ 
	"branchName",
	"latitude", 
	"longitude", 
	"streetAddress", 
	"city",
	"countrySubDvision",
	"country",
	"postCode" 
	})

public class CustomBranchLocation {
	@JsonProperty("branchName")
	private String branchName;

	@JsonProperty("latitude")
	private Float latitude;

	@JsonProperty("longitude")
	private Float longitude;

	@JsonProperty("streetAddress")
	private List<String> streetAddress;

	@JsonProperty("city")
	private String city;

	@JsonProperty("countrySubDivision")
	private List<String> countrySubDivision;

	@JsonProperty("country")
	private String country;

	@JsonProperty("postCode")
	private String postCode;

	@JsonProperty("branchName")
	public String getBranchName() {
		return branchName;
	}

	@JsonProperty("branchName")
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@JsonProperty("latitude")
	public Float getLatitude() {
		return latitude;
	}

	@JsonProperty("latitude")
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	@JsonProperty("longitude")
	public Float getLongitude() {
		return longitude;
	}

	@JsonProperty("longitude")
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	@JsonProperty("streetAddress")
	public List<String> getStreetAddress() {
		return streetAddress;
	}

	@JsonProperty("streetAddress")
	public void setStreetAddress(List<String> streetAddress) {
		this.streetAddress = streetAddress;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("countrySubDivision")
	public List<String> getCountrySubDivision() {
		return countrySubDivision;
	}

	@JsonProperty("countrySubDivision")
	public void setCountrySubDivision(List<String> countrySubDivision) {
		this.countrySubDivision = countrySubDivision;
	}

	@JsonProperty("country")
	public String getCountry() {
		return country;
	}

	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	@JsonProperty("postCode")
	public String getPostCode() {
		return postCode;
	}

	@JsonProperty("postCode")
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

}
