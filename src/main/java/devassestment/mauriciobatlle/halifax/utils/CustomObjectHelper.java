package devassestment.mauriciobatlle.halifax.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import devassestment.mauriciobatlle.halifax.beans.CustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.CustomWSResponseEntity;
import devassestment.mauriciobatlle.halifax.beans.Branch;
import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;
import devassestment.mauriciobatlle.halifax.beans.PostalAddress;
import devassestment.mauriciobatlle.halifax.utils.interfaces.BranchMapper;

@Component
public class CustomObjectHelper {
	//this functionality is now handled by the branch mapper Interface.
	@Deprecated
	public static CustomBranchLocation createCustomBranchLocation(Branch halifaxBranch) {
		PostalAddress branchPostalInfo = halifaxBranch.getPostalAddress();
		CustomBranchLocation newBranch = new CustomBranchLocation();
		newBranch.setBranchName(halifaxBranch.getName());
		newBranch.setCity(branchPostalInfo.getTownName());
		newBranch.setCountry(branchPostalInfo.getCountry());
		newBranch.setCountrySubDivision(branchPostalInfo.getCountrySubDivision());
		newBranch.setLatitude(
				Float.parseFloat(branchPostalInfo.getGeoLocation().getGeographicCoordinates().getLatitude()));
		newBranch.setLongitude(
				Float.parseFloat(branchPostalInfo.getGeoLocation().getGeographicCoordinates().getLongitude()));
		newBranch.setPostCode(branchPostalInfo.getPostCode());
		newBranch.setStreetAddress(branchPostalInfo.getAddressLine());
		return newBranch;
	}

	public static void applyCityFilter(String city, OriginalResponse rawResponse) {
		List<Branch> branchList = rawResponse.getData().get(0).getBrand().get(0).getBranch();
		ListIterator<Branch> iterator = branchList.listIterator();
		while (iterator.hasNext()) {
			Branch currentBranch = iterator.next();
			if ((currentBranch.getPostalAddress().getTownName() == null)
					|| (!currentBranch.getPostalAddress().getTownName().equalsIgnoreCase(city))
					|| (currentBranch.getPostalAddress().getTownName().isEmpty())) {
				iterator.remove();
			}

		}

	}

	public static CustomWSResponseEntity createCustomWsResponse(OriginalResponse origResponse) {
		CustomWSResponseEntity response = new CustomWSResponseEntity();
		List<CustomBranchLocation> locationElements = new LinkedList<CustomBranchLocation>();
		List<Branch> originalLocationList = origResponse.getData().get(0).getBrand().get(0).getBranch();
		ListIterator<Branch> originalLocationIterator = originalLocationList.listIterator();
		while (originalLocationIterator.hasNext()) {
			locationElements.add(BranchMapper.INSTANCE.halifaxToCustomBranch(originalLocationIterator.next()));

//			locationElements.add(createAavriBranchLocation(originalLocationIterator.next()));
		}
		response.setBranchLocations(locationElements);
		return response;
	}
}
