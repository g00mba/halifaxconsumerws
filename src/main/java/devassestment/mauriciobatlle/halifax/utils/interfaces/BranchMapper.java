package devassestment.mauriciobatlle.halifax.utils.interfaces;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import devassestment.mauriciobatlle.halifax.beans.CustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.Branch;

@Mapper
public interface BranchMapper {
	BranchMapper INSTANCE = Mappers.getMapper(BranchMapper.class);
	@Mappings({
		@Mapping(source="name",target = "branchName"),
		@Mapping(source="postalAddress.townName",target = "city"),
		@Mapping(source="postalAddress.country",target = "country"),
		@Mapping(source="postalAddress.countrySubDivision",target = "countrySubDivision"),
		@Mapping(source="postalAddress.geoLocation.geographicCoordinates.latitude",target = "latitude"),
		@Mapping(source="postalAddress.geoLocation.geographicCoordinates.longitude",target = "longitude"),
		@Mapping(source="postalAddress.postCode",target = "postCode"),
		@Mapping(source="postalAddress.addressLine",target="streetAddress")
	})
	
	CustomBranchLocation halifaxToCustomBranch(Branch branch);
}
