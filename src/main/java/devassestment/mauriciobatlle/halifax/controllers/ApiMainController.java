package devassestment.mauriciobatlle.halifax.controllers;

import static devassestment.mauriciobatlle.halifax.datasource.Branches.getObjectFromWS;
import static devassestment.mauriciobatlle.halifax.utils.CustomObjectHelper.createCustomWsResponse;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import static devassestment.mauriciobatlle.halifax.utils.CustomObjectHelper.applyCityFilter;
import devassestment.mauriciobatlle.halifax.beans.CustomWSResponseEntity;
import devassestment.mauriciobatlle.halifax.beans.OriginalResponse;

@RestController
public class ApiMainController {
	@Cacheable(value = "processedResponse", sync = true)
	@GetMapping(value = "/api/V0.1/branches")
	public CustomWSResponseEntity branchesStandardEndpoint() {
		OriginalResponse source = getObjectFromWS();
		return createCustomWsResponse(source);
	}

	@Cacheable(value = "filteredResponse", sync = true)
	@GetMapping(value = "/api/V0.1/branches/filter/city/{city}")
	public CustomWSResponseEntity branchesCityFilteredEndpoint(@PathVariable String city) {
		OriginalResponse source = getObjectFromWS();
		applyCityFilter(city, source);
		return createCustomWsResponse(source);
	}

}
