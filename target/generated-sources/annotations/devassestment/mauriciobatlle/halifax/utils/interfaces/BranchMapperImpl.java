package devassestment.mauriciobatlle.halifax.utils.interfaces;

import devassestment.mauriciobatlle.halifax.beans.Branch;
import devassestment.mauriciobatlle.halifax.beans.CustomBranchLocation;
import devassestment.mauriciobatlle.halifax.beans.GeoLocation;
import devassestment.mauriciobatlle.halifax.beans.GeographicCoordinates;
import devassestment.mauriciobatlle.halifax.beans.PostalAddress;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-07-20T17:06:59-0700",
    comments = "version: 1.3.0.Beta2, compiler: Eclipse JDT (IDE) 3.22.0.v20200530-2032, environment: Java 1.8.0_252 (Private Build)"
)
public class BranchMapperImpl implements BranchMapper {

    @Override
    public CustomBranchLocation halifaxToCustomBranch(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        CustomBranchLocation customBranchLocation = new CustomBranchLocation();

        String country = branchPostalAddressCountry( branch );
        if ( country != null ) {
            customBranchLocation.setCountry( country );
        }
        List<String> countrySubDivision = branchPostalAddressCountrySubDivision( branch );
        List<String> list = countrySubDivision;
        if ( list != null ) {
            customBranchLocation.setCountrySubDivision( new ArrayList<String>( list ) );
        }
        String townName = branchPostalAddressTownName( branch );
        if ( townName != null ) {
            customBranchLocation.setCity( townName );
        }
        List<String> addressLine = branchPostalAddressAddressLine( branch );
        List<String> list1 = addressLine;
        if ( list1 != null ) {
            customBranchLocation.setStreetAddress( new ArrayList<String>( list1 ) );
        }
        String latitude = branchPostalAddressGeoLocationGeographicCoordinatesLatitude( branch );
        if ( latitude != null ) {
            customBranchLocation.setLatitude( Float.parseFloat( latitude ) );
        }
        customBranchLocation.setBranchName( branch.getName() );
        String postCode = branchPostalAddressPostCode( branch );
        if ( postCode != null ) {
            customBranchLocation.setPostCode( postCode );
        }
        String longitude = branchPostalAddressGeoLocationGeographicCoordinatesLongitude( branch );
        if ( longitude != null ) {
            customBranchLocation.setLongitude( Float.parseFloat( longitude ) );
        }

        return customBranchLocation;
    }

    private String branchPostalAddressCountry(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        String country = postalAddress.getCountry();
        if ( country == null ) {
            return null;
        }
        return country;
    }

    private List<String> branchPostalAddressCountrySubDivision(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        List<String> countrySubDivision = postalAddress.getCountrySubDivision();
        if ( countrySubDivision == null ) {
            return null;
        }
        return countrySubDivision;
    }

    private String branchPostalAddressTownName(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        String townName = postalAddress.getTownName();
        if ( townName == null ) {
            return null;
        }
        return townName;
    }

    private List<String> branchPostalAddressAddressLine(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        List<String> addressLine = postalAddress.getAddressLine();
        if ( addressLine == null ) {
            return null;
        }
        return addressLine;
    }

    private String branchPostalAddressGeoLocationGeographicCoordinatesLatitude(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        GeoLocation geoLocation = postalAddress.getGeoLocation();
        if ( geoLocation == null ) {
            return null;
        }
        GeographicCoordinates geographicCoordinates = geoLocation.getGeographicCoordinates();
        if ( geographicCoordinates == null ) {
            return null;
        }
        String latitude = geographicCoordinates.getLatitude();
        if ( latitude == null ) {
            return null;
        }
        return latitude;
    }

    private String branchPostalAddressPostCode(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        String postCode = postalAddress.getPostCode();
        if ( postCode == null ) {
            return null;
        }
        return postCode;
    }

    private String branchPostalAddressGeoLocationGeographicCoordinatesLongitude(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        GeoLocation geoLocation = postalAddress.getGeoLocation();
        if ( geoLocation == null ) {
            return null;
        }
        GeographicCoordinates geographicCoordinates = geoLocation.getGeographicCoordinates();
        if ( geographicCoordinates == null ) {
            return null;
        }
        String longitude = geographicCoordinates.getLongitude();
        if ( longitude == null ) {
            return null;
        }
        return longitude;
    }
}
